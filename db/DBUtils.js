const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'Inge3.db');
const db = new sqlite3.Database(dbPath);

// TODO Para verificar que la conexión con la base de datos sea exitosa
// db.serialize(() => {
//     db.run('SELECT 1', [], (err) => {
//         if (err) {
//             console.error('Error al conectar con la base de datos:', err);
//         } else {
//             console.log('Conexión exitosa con la base de datos.');
//         }
//     });
// });

// Función para obtener todos los integrantes
function getIntegrantes() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM integrantes WHERE activo = 1 order by orden', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
// Función para obtener un integrante por matricula
function getIntegranteByMatricula(matricula) {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM integrantes WHERE activo = 1 AND matricula = ?', [matricula], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
}

// Función para obtener todos los medios por matricula
function getMediaByMatricula(matricula) {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM media WHERE activo = 1 AND matricula = ?', [matricula], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
// Función para obtener los datos de home
function getHome() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM home', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

// Función para obtener los datos de informacion
function getInformacion() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM informacion', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

// Función para obtener los datos de tipoMedia
function getTipoMedia() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM tipo_media WHERE activo = 1 order by orden', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
// Función para obtener todos los integrantes
function getAllIntegrantes() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM integrantes order by orden asc', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
// Función para crear un integrante
function createIntegrante(integrante) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO integrantes (nombre, apellido, matricula, descripcion, orden, activo) VALUES (?, ?, ?, ?, ?, ?)', [integrante.nombre, integrante.apellido, integrante.matricula, integrante.descripcion, integrante.orden, integrante.activo], (err) => {
            if (err) {
                console.log('Error al crear el integrante:', err);
                reject(err);
            } else {
                resolve();
                console.log('El integrante ha sido creado correctamente.');
            }
        });
    });
}
// Función para crear un tipo media
function createTipoMedia(tipoMedia) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO tipo_media (nombre, activo, orden) VALUES (?, ?, ?)', [tipoMedia.nombre, tipoMedia.activo, tipoMedia.orden], (err) => {
            if (err) {
                console.log('Error al crear el tipo media:', err);
                reject(err);
            } else {
                resolve();
                console.log('El tipo media ha sido creado correctamente.');
            }
        });
    });
}
//listado de media que traiga con el nombre del integrante y nombre de tipo media, es decir un join para traer los datos de las tablas relacionadas
function getMedia() {
    return new Promise((resolve, reject) => {
        db.all('SELECT m.*, i.nombre as nombreIntegrante, tm.nombre as nombreTipoMedia FROM media m JOIN integrantes i ON m.matricula = i.matricula JOIN tipo_media tm ON m.tipo_media_id = tm.id', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
//Crear una media
function createMedia(media, destino) {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO media (matricula, tipo_media_id, titulo, alt, orden, url, src, activo) VALUES (?,?,?,?,?,?,?,?)', [media.integrante, media.tipo_media, media.titulo, media.descripcion, media.orden, media.url, destino, media.activo], (err) => {
            if (err) {
                console.log('Error al crear la media:', err);
                reject(err);
            } else {
                resolve();
                console.log('La media ha sido creada correctamente.');
            }
        });
    });
}


// Exportar las funciones
module.exports = {
    getIntegrantes,
    getMediaByMatricula,
    getHome,
    getInformacion,
    getTipoMedia,
    getIntegranteByMatricula,
    getAllIntegrantes,
    createIntegrante,
    createTipoMedia,
    getMedia,
    createMedia,
};