const express = require("express");
const router = express.Router();
const db = require('../db/DBUtils');
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/assets/' });
const fileUpload = upload.single('imagen');


router.get("/", (req, res) => {
    res.render("admin/index");
});

router.get("/integrantes/listar", (req, res) => {
    db.getAllIntegrantes().then((integrantes) => {
        res.render("admin/integrantes", { integrantes });
    }).catch((err) => {
        res.render("admin/integrantes", { error: err });
    });
});

router.get("/integrantes/crear", (req, res) => {
    res.render("admin/integrantes/Crear");
});

router.post("/integrantes/create", (req, res) => {
    if (req.body) {
        const integrante = req.body;
        // Validación de los campos del formulario
        if (!integrante.nombre || !integrante.apellido || !integrante.matricula || !integrante.descripcion || !integrante.orden || !integrante.activo) {
            return res.render("admin/integrante/Crear", { error: "Los campos son obligatorios" });
        }
        // Creación del integrante
        db.createIntegrante(integrante).then(() => {
            res.redirect("/admin/integrantes/listar?success=Integrante creado con éxito");
        }).catch((err) => {
            res.render("admin/integrantes/Crear", { error: err });
        });
    } else {
        res.render("admin/integrantes/Crear", { error: "No se recibieron datos del formulario" });
    }
});
//listar tipo media
router.get("/tipo_media/listar", (req, res) => {
    db.getTipoMedia().then((tipoMedia) => {
        res.render("admin/tipo_media", { tipoMedia });
    }).catch((err) => {
        res.render("admin/tipo_media", { error: err });
    });
});
router.get("/tipo_media/crear", (req, res) => {
    res.render("admin/tipo_media/Crear");
});
router.post("/tipo_media/create", (req, res) => {
    if (req.body) {
        const tipoMedia = req.body;
        // Validación de los campos del formulario
        if (!tipoMedia.nombre || !tipoMedia.orden || !tipoMedia.activo) {
            return res.render("admin/tipo_media/Crear", { error: "Los campos son obligatorios" });
        }

        // Creación del tipo media
        db.createTipoMedia(tipoMedia).then(() => {
            // como no se puede redirigir a una vista, se redirige a la lista de tipo media para ver el nuevo tipo media creado con éxito
            res.redirect("/admin/tipo_media/listar?success=Tipo de media creado con éxito");
        }).catch((err) => {
            res.render("admin/tipo_media/Crear", { error: err });
        });
    } else {
        res.render("admin/tipo_media/Crear", { error: "No se recibieron datos del formulario" });
    }
});
//lista media
router.get("/media/listar", (req, res) => {
    db.getMedia().then((media) => {
        res.render("admin/media/index", { media });
    }).catch((err) => {
        res.render("admin/media/index", { error: err });
    });
});
//crear media
router.get("/media/crear", (req, res) => {
    Promise.all([db.getAllIntegrantes(), db.getTipoMedia()])
        .then(([integrantes, tipoMedia]) => {
            res.render("admin/media/Crear", { integrantes, tipoMedia });
        })
        .catch((err) => {
            res.render("admin/media/Crear", { error: err });
        });
});

router.post("/media/create", fileUpload, (req, res) => {
    if (req.body) {
        const media = req.body;

        // Verificar si se subió un archivo
        let tmp_path, destino;
        if (req.file) {
            tmp_path = req.file.path;
            destino = "./public/assets/" + req.file.originalname;
            fs.rename(tmp_path, destino, (err) => {
                if (err) {
                    console.log(err);
                    return res.render("admin/media/Crear", { error: "No se pudo guardar la imagen" });
                }
            });
        }
        // Validación de los campos del formulario
        if (!media.integrante ||!media.tipo_media ||!media.orden ||!media.activo ||!media.titulo ||!media.descripcion) {
            return res.render("admin/media/Crear", { error: "Los campos son obligatorios" });
        }

        // Crear media sin la imagen si no se subió ninguno
        db.createMedia(media, req.file? '/assets/'+req.file.originalname : null).then(() => {
            res.redirect(`/admin/media/listar?success=Media creada con éxito`);
        }).catch((err) => {
            res.render("admin/media/Crear", { error: err });
        });
    } else {
        res.render("admin/media/Crear", { error: "No se recibieron datos del formulario" });
    }
});



module.exports = router;
