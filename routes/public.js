const express = require('express');
const data = require("../db/DBUtils");
const getIntegrantes = data.getIntegrantes;
const getMediaByMatricula = data.getMediaByMatricula;
const getHome = data.getHome;
const getInformacion = data.getInformacion;
const getTipoMedia = data.getTipoMedia;
const getIntegranteByMatricula = data.getIntegranteByMatricula;

require('dotenv').config();
const router = express.Router();

router.use(async (req, res, next) => {
    try {
        res.locals.integrantes = await getIntegrantes();
        next();
    } catch (err) {
        next(err);
    }
});

router.use((req, res, next) => {
    res.locals.gitlabUrl = process.env.GITLAB_URL;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.matricula = process.env.MATRICULA;
    res.locals.materia = process.env.MATERIA;
    next();
});

router.get("/", async (req, res, next) => {
    try {
        const home = await getHome();
        const informacion = await getInformacion();
        res.render('index', {
            home: home[0],
            curso: informacion[0],
            wordCloud: informacion[1],
            isHome: true
        });
    } catch (err) {
        next(err);
    }
});

router.get("/curso", (req, res) => {
    res.render('curso');
});

router.get("/word_cloud", (req, res) => {
    res.render('word_cloud');
});

router.get("/:matricula", async (req, res, next) => {
    try {
        const matricula = req.params.matricula;
        const integrante = await getIntegranteByMatricula(matricula);
        if (integrante) {
            res.locals.integrante = [integrante];
            next();
        } else {
            res.status(404).render('404_page');
        }
    } catch (err) {
        next(err);
    }
}, async (req, res, next) => {
    try {
        const matricula = req.params.matricula;
        const mediaFiltrada = await getMediaByMatricula(matricula);
        const tipoMedia = await getTipoMedia();
        res.render('Integrante', {
            tipoMedia: tipoMedia,
            media: mediaFiltrada,
            integrante: res.locals.integrante
        });
    } catch (err) {
        next(err);
    }
});

module.exports = router;