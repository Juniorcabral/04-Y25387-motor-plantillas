# Configurar y crear la bd con SQLite

Por problemas al crear la bd por medio de la consola de SQLite, se optó por crear la bd y la tabla por medio de un script llamado CreateDB.js.

## Pasos realizados

1. `npm install sqlite3` para instalar las dependencias.
2. se creó y se configuró el archivo `CreateDB.js` luego de una ardua investigación, depuración y café.
3. Navegar a la carpeta donde se encuentra el archivo `CreateDB.js`
4. `node CreateDB.js` para crear la base de datos y la tabla.
